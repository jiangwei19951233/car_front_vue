// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSouece from 'vue-resource'
import store from '@/store'

// in ES6 modules
import { Swipe, SwipeItem } from 'vue-swipe'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'

Vue.use(VueSouece)
Vue.use(MintUI)

require('vue-swipe/dist/vue-swipe.css')

Vue.component('swipe', Swipe)
Vue.component('swipe-item', SwipeItem)

Vue.config.productionTip = false

// function GetRequest () {
//   var url = window.location.hash.split('?')[1]; // 获取url中'?'符后的字串
//   console.log(url)
//   var theRequest = new Object()
//   if (url.indexOf('?') !== -1) {
//     var str = url.substr(1)
//     strs = str.split('&')
//     for (var i = 0; i < strs.length; i++) {
//       theRequest[strs[i].split('=')[0]] = unescape(strs[i].split('=')[1])
//     }
//   }
//   return theRequest
// }

window.openid = (function getQueryString (name) {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  var r = window.location.hash.split('?')[1].match(reg)
  return r != null ? unescape(r[2]) : null
})('openid')
// window.openid = window.openid ? window.openid : '5941e0b9e7649160d53ec043'

// wx.config({
//     debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
//     appId: '', // 必填，公众号的唯一标识
//     timestamp: '', // 必填，生成签名的时间戳
//     nonceStr: '', // 必填，生成签名的随机串
//     signature: '',// 必填，签名，见附录1
//     jsApiList: [
//       'chooseImage'
//     ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
// });

// wx.hideOptionMenu()
// wx.hideAllNonBaseMenuItem()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  mounted: function () {
    setTimeout(function () {
      if (typeof WeixinJSBridge === 'undefined') {
      } else {
        WeixinJSBridge.call('hideOptionMenu')
      }
    }, 2000)
  }
})
