import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Home from '@/components/Pages/Home/Home'
import Near from '@/components/Pages/Near/Near'
import Serv from '@/components/Pages/Serv/Serv'
import Mine from '@/components/Pages/Mine/Mine'
import SelfDriving from '@/components/PagesOfFunctions/SelfDriving/SelfDriving'
import SelectCarModel from '@/components/PagesOfFunctions/SelectCarModel/SelectCarModel'
import ConfirmOrder from '@/components/PagesOfFunctions/ConfirmOrder/ConfirmOrder'
import Invoice from '@/components/PagesOfFunctions/Invoice/Invoice'
import InvoiceAdd from '@/components/PagesOfFunctions/InvoiceAdd/InvoiceAdd'
import SelfDrivingOrder from '@/components/Pages/SelfDrivingOrder/SelfDrivingOrder'
import SelfDrivingOrderInfo from '@/components/Pages/SelfDrivingOrderInfo/SelfDrivingOrderInfo'
import StoresList from '@/components/Pages/StoresList/StoresList'
import Stores from '@/components/Pages/Near/Stores/Stores'
import IdIdentify from '@/components/Pages/IdIdentify/IdIdentify'
import IdIdentifyOk from '@/components/Pages/IdIdentify/Ok/Ok'
import authentication from '@/components/Pages/Mine/Methods/authentication'
import selfMsg from '@/components/Pages/Mine/Methods/selfmsg'
import aggrement from '@/components/Pages/Mine/Methods/aggrement'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/near',
      name: 'Near',
      component: Near
    },
    {
      path: '/serv',
      name: 'Serv',
      component: Serv
    },
    {
      path: '/mine',
      name: 'Mine',
      component: Mine
    },
    {
      path: '/selfdriving',
      name: 'SelfDriving',
      component: SelfDriving
    },
    {
      path: '/selectcarmodel',
      name: 'SelectCarModel',
      component: SelectCarModel
    },
    {
      path: '/confirmorder', // 确认订单
      name: 'ConfirmOrder',
      component: ConfirmOrder
    },
    {
      path: '/selfdrivingorder',
      name: 'SelfDrivingOrder',
      component: SelfDrivingOrder
    },
    {
      path: '/selfdrivingorderinfo',
      name: 'SelfDrivingOrderInfo',
      component: SelfDrivingOrderInfo
    },
    {
      path: '/stores',
      name: 'Stores',
      component: Stores
    },
    {
      path: '/storeslist',
      name: 'StoresList',
      component: StoresList
    },
    {
      path: '/invoiceadd',
      name: 'InvoiceAdd',
      component: InvoiceAdd
    },
    {
      path: '/invoice',
      name: 'Invoice',
      component: Invoice
    },
    {
      path: '/ididentify',
      name: 'IdIdentify',
      component: IdIdentify
    },
    {
      path: '/ididentifyok',
      name: 'IdIdentifyOk',
      component: IdIdentifyOk
    },
    {
      path: '/users/authentication',
      name: 'authentication',
      component: authentication
    },
    {
      path: '/users/selfmsg',
      name: 'selfmsg',
      component: selfMsg
    },
    {
      path: '/users/aggrement',
      name: 'aggrement',
      component: aggrement
    }
  ]
})
