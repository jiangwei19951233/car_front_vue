import fetch from '../axios'
import type from './type'

const actions = {
  [type.GET_USER] ({ state }, { openid }) {
    return new Promise((resolve, reject) => {
      fetch.get(`/users/${openid}`).then(res => {
        state.user = res.data.result
        resolve(res)
      })
    })
  },
  [type.SEND_MSG] ({ state }, { phone, type }) {
    return new Promise((resolve, reject) => {
      fetch.post(`/messages/send`, { phone, type }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [type.USER_VERIFY] ({ state }, params) {
    return new Promise((resolve, reject) => {
      fetch.post(`/messages/verify`, params).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [type.PULL_AGGREMENT] ({ state }) {
    return fetch.get('/aggrements')
  }
}

export default actions
